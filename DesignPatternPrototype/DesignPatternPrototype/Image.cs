﻿namespace DesignPatternPrototype
{
    /// <summary>
    /// Класс описания изображений.
    /// </summary>
    public class Image : Content, IMyCloneable<Image>
    {
        /// <summary>
        /// Высота изображения, px.
        /// </summary>
        private int Height { get; set; }

        /// <summary>
        /// Ширина изображения, px.
        /// </summary>
        private int Width { get; set; }

        public Image(int id, string name, int size, int height, int width)
            : base(id, name, size)
        {
            Height = height;
            Width = width;
        }

        public override object Clone() =>
            Copy();

        public Image Copy() =>
            new Image(Id, Name, Size, Height, Width);

        public override string ToString() =>
            $"{nameof(Id)}: {Id}, {nameof(Name)}: {Name}, {nameof(Size)}: {Size}, " +
                $"{nameof(Height)}: {Height}, {nameof(Width)}: {Width}";
    }
}
