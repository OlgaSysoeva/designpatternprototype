﻿using System;

namespace DesignPatternPrototype
{
    /// <summary>
    /// Класс описания общих параметров контента сайта.
    /// </summary>
    public class Content : IMyCloneable<Content>, ICloneable
    {
        /// <summary>
        /// Идентификатор.
        /// </summary>
        private protected int Id { get; set; }

        /// <summary>
        /// Название контента.
        /// </summary>
        private protected string Name { get; set; }

        /// <summary>
        /// Размер контента, Кб.
        /// </summary>
        public int Size { get; set; }

        public Content(int id, string name, int size)
        {
            Id = id;
            Name = name;
            Size = size;
        }

        public virtual object Clone() =>
            Copy();

        public Content Copy() =>
            new Content(Id, Name, Size);

        public override string ToString() =>
            $"{nameof(Id)}: {Id}, {nameof(Name)}: {Name}, {nameof(Size)}: {Size}";
    }
}
