﻿namespace DesignPatternPrototype
{
    /// <summary>
    /// Класс описания видео.
    /// </summary>
    public class Video : Content, IMyCloneable<Video>
    {
        /// <summary>
        /// Ширина кадра.
        /// </summary>
        private int FrameWidth { get; set; }

        /// <summary>
        /// Высота кадра.
        /// </summary>
        private int FrameHeight { get; set; }

        public Video(int id, string name, int size, int frameWidth, int frameHeight)
            : base(id, name, size)
        {
            FrameWidth = frameWidth;
            FrameHeight = frameHeight;
        }

        public override object Clone() =>
            Copy();

        public Video Copy() =>
            new Video(Id, Name, Size, FrameWidth, FrameHeight);

        public override string ToString() =>
            $"{nameof(Id)}: {Id}, {nameof(Name)}: {Name}, {nameof(Size)}: {Size}, " +
                $"{nameof(FrameWidth)}: {FrameWidth},{nameof(FrameHeight)}: {FrameHeight}";
    }
}
