﻿namespace DesignPatternPrototype
{
    public interface IMyCloneable<T> where T : class
    {
        /// <summary>
        /// Делает копию самого себя.
        /// </summary>
        /// <returns>Возвращает полную копию себя.</returns>
        T Copy();
    }
}
