﻿using System;

namespace DesignPatternPrototype
{
    class Program
    {
        static void Main()
        {
            var content = new Content(1, "Веселая песенка", 123);
            Console.WriteLine("Новый объект Content:");
            Console.WriteLine(content.ToString());

            var contentCopy = content.Copy();
            Console.WriteLine("Копия объекта Content через IMyCloneable:");
            Console.WriteLine(contentCopy.ToString());

            var contentClone = (Content)content.Clone();
            Console.WriteLine("Копия объекта Content через ICloneable:");
            Console.WriteLine(contentClone.ToString());

            Console.WriteLine("\n");

            var image = new Image(2, "Природа", 319, 1_200, 1_600);
            Console.WriteLine("Новый объект Image:");
            Console.WriteLine(image.ToString());

            var imageCopy = image.Copy();
            Console.WriteLine("Копия объекта Image через IMyCloneable:");
            Console.WriteLine(imageCopy.ToString());

            var imageClone = (Image)image.Clone();
            Console.WriteLine("Копия объекта Image через ICloneable:");
            Console.WriteLine(imageClone.ToString());

            Console.WriteLine("\n");

            var video = new Video(3, "Новый год", 120_564, 640, 352);
            Console.WriteLine("Новый объект Video:");
            Console.WriteLine(video.ToString());

            var videoCopy = video.Copy();
            Console.WriteLine("Копия объекта Video через IMyCloneable:");
            Console.WriteLine(videoCopy.ToString());

            var videoClone = (Video)video.Clone();
            Console.WriteLine("Копия объекта Video через ICloneable:");
            Console.WriteLine(videoClone.ToString());
        }

        /*
         * Выводы:
         * Наследование интерфейсов у родительских классов приводит 
         *      к переопределению метода у каждого дочернего класса.
         *      
         * Использование собственного интерфейса клонирования позволяет более точно настраивать этот
         *      процесс и применять нужную глубину копирования. При необходимости можно реализовать разные методы с 
         *      разной глубиной копирования.
         *      
         * Так же при использовании собственного интерфейса не приходится производить приведение типов (если делать его дженериком), 
         *      что производительнее.
         * 
         * IClonable может быть уже реализован в стандартных классах, что упрощает разработку.
         */
    }
}
